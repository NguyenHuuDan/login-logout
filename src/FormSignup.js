import React from 'react';
import homeForm from'./homeForm';
import validate from './nav' ;
import './Form.css';

const FormSignup=({submitForm})=>{
    const{handleChange,values,handleSubmit,errors}=homeForm(submitForm,validate);
    return(
        <div className="form-content-right">
            <form className="form"  onSubmit={handleSubmit}>
                <h1>SignUp MyAppStore</h1>
                <div className="form-input">
                    <label htmlFor="email" className="form-label">Email-Số điện thoại  </label>
                        <input type="text" name="email" className="form-input" placeholder="Nhập Email:" value={values.email} onChange={handleChange}/>
                        {errors.email && <p>{errors.email}</p>}
                  
                </div>
                <div className="form-input">
                    <label htmlFor="password" className="form-label">Mật khẩu  </label>
                        <input type="text" name="password" className="form-input" placeholder="Nhập mật khẩu:"value={values.password} onChange={handleChange}/>
                        {errors.password && <p>{errors.password}</p>}
                </div>
                <div className="form-input">
                    <label htmlFor="confirmpassword" className="form-label">Xác nhận mật khẩu  </label>
                        <input type="text" name="confirmpassword" className="form-input" placeholder="Xác nhận mật khẩu:"value={values.confirmpassword} onChange={handleChange}/>
                        {errors.confirmpassword && <p>{errors.confirmpassword}</p>}
                </div>
                <div className="form-input">
                    <label htmlFor="address" className="form-label">Địa chỉ </label>
                        <input type="text" name="address" className="form-input" placeholder="Địa chỉ:"value={values.address} onChange={handleChange}/>
                        {errors.address && <p>{errors.address}</p>}
                </div>
            </form>
<button className="btn btn-pimary" type="submit">Đăng Kí</button> 
<span className="form-login">Nếu bạn đã có tài khoản? Đăng nhập ngay<a href="#"     > Tại đây   </a></span>
        </div>
    )
}
export default FormSignup