import React, {useState} from 'react'
import FormSignup from './FormSignup'
import FormLogin from './FormLogin'
import './Form.css'

const Form = () => {
    const [isSubmitted, setisSubmitted]= useState(false)
    function submitForm(){
        setisSubmitted(true)
    }
    return (
        <div>
            <FormSignup />
            {isSubmitted ?<FormSignup submitForm={submitForm}/>:<FormLogin/>}
        </div>
    )
}

export default Form
