export default function nav(values){
    let errors={}
    if(!values.email.trim()){
        errors.email="Email Required"
    }
    if(!values.password.trim()){
        errors.password="Password Required"
    }else if(values.password.length<8){
        errors.password="Mật khẩu cần phải 8 kí tự trở lên!"
    }
    if(!values.confirmpassword.trim()){
        errors.confirmpassword="Confirmpassword Required"
    }else if(values.confirmpassword !==values.password){
        errors.password="Mật khẩu không khớp!"
    }
    if(!values.address.trim()){
        errors.address="Address Required"
    }
    return errors;

}